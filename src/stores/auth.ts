import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 1,
    email: 'mix555@gmail.com',
    password: '1144',
    fullName: 'mix 555',
    gender: 'male',
    roles: ['user']
})

  return { currentUser }
})
